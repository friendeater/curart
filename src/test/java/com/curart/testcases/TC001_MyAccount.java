package com.curart.testcases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.curart.pageobjects.AuthPage;
import com.curart.pageobjects.HomePage;
import com.curart.utilities.XLUtils;

public class TC001_MyAccount extends BaseClass
{

	@Test(dataProvider="LoginInfo")
	public void existingUser(String emailID, String pwd, String sb_product, String bb_product) throws InterruptedException
	{
		logger.info("****** ExistingUser Test starting********");
		
		HomePage hp = new HomePage(driver);									
		hp.acceptCookies();
		Thread.sleep(2000);
		hp.clickSignInLink();
		
		AuthPage ap = new AuthPage(driver);
		ap.getEmail(emailID);
		ap.getPassword(pwd);
		ap.clickSubmit();
		
		String welcometext = driver.findElement(By.xpath("//span[@class='dc-heading-text' and contains(text(),'Welcome')]")).getText();
		Assert.assertEquals(welcometext, "Welcome, vijay");
		logger.info("****** ExistingUser Test ended********");
	}
	
//	@Test(dataProvider="LoginInfo")
//	public void verifyPersonalDetails(String emailID, String pwd) throws InterruptedException
//	{
//		logger.info("****** PD Test starting********");
//		
//		HomePage hp = new HomePage(driver);									
//		hp.acceptCookies();
//		Thread.sleep(2000);
//		hp.clickSignInLink();
//		
//		AuthPage ap = new AuthPage(driver);
//		ap.getEmail(emailID);
//		ap.getPassword(pwd);
//		ap.clickSubmit();
//		
//		String welcometext = driver.findElement(By.xpath("//span[@class='dc-heading-text' and contains(text(),'Welcome')]")).getText();
//		Assert.assertEquals(welcometext, "Welcome, vijay");
//		
//		
//		MyAccount ma = new MyAccount(driver);
//		ma.pdclick();
//		String MyAccountTitle = driver.getTitle();
//		Assert.assertEquals(MyAccountTitle, "My Account - Home | Currys");
//		Assert.assertTrue(driver.findElement(By.xpath("//*[@title='Personal details']")).isDisplayed());
//
//		logger.info("****** Pd Test ended********");
//	}

	
	@DataProvider(name = "LoginInfo")
	public String[][] getLoginData() throws IOException
	{
		String path = "./src/test/java/com/curart/testdata/testdata.xlsx";
		
		int rownum = XLUtils.getRowCount(path, "logindata");
		int colnum = XLUtils.getColCount(path, "logindata", 1);
		
		String[][] dataArray = new String[rownum][colnum];
		
		for(int i=1;i<=rownum;i++)
		{
			for(int j=0;j<colnum;j++)
			{
				
				dataArray[i-1][j] = XLUtils.getCellData(path, "logindata", i, j);
			}
		}
		return dataArray;
	}
	
	
	
}
