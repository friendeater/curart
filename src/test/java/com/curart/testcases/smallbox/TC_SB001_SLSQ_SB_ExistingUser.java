package com.curart.testcases.smallbox;

import java.io.IOException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.curart.pageobjects.BasketPage;
import com.curart.pageobjects.CheckoutPage;
import com.curart.pageobjects.MyAccountPage;
import com.curart.pageobjects.PaymentPage;
import com.curart.pageobjects.ProductPage;
import com.curart.testcases.BaseClass;
import com.curart.utilities.XLUtils;

public class TC_SB001_SLSQ_SB_ExistingUser extends BaseClass {

	@Test(dataProvider = "Products")
	public void slsq_withoutServices(String email, String pwd, String sb, String bb) throws InterruptedException
	{
		logger.info("**** TC_SB001_SLSQ_SB_ExistingUser STARTING");
		
		MyAccountPage ma = new MyAccountPage(signIn());
		Thread.sleep(3000);
		ma.searchQuery(sb);
		ma.searchSubmitClick();
		
		ProductPage pp = new ProductPage(driver);
		pp.addToBasketClick();
		pp.continueToBasket();
		
		BasketPage bp = new BasketPage(driver);
		bp.goToCheckoutClick();
		
		CheckoutPage cp = new CheckoutPage(driver);
		cp.flexibleFreeDeliveryClick();
		Thread.sleep(3000);
		//cp.changeLocationclick();
		//cp.postcodeSearch();
		//cp.confirmAndContinueClick();
		cp.creditCardSelect();
		Thread.sleep(3000);
		
		PaymentPage paymentPage = new PaymentPage(driver);
		paymentPage.cancelPaymentButtonClick();
	}
	
	
	
	@DataProvider(name = "Products")
	public String[][] getData() throws IOException
	{
		String path = "./src/test/java/com/curart/testdata/testdata.xlsx";
		
		int rownum = XLUtils.getRowCount(path, "logindata");
		int colnum = XLUtils.getColCount(path, "logindata", 1);
		
		String[][] dataArray = new String[rownum][colnum];
		
		for(int i=1;i<=rownum;i++)
		{
			for(int j=0;j<colnum;j++)
			{
				dataArray[i-1][j]=XLUtils.getCellData(path, "logindata", i, j);
			}
		}
		
		return dataArray;
	}
	
	
}
