package com.curart.testcases;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentTest;
import com.curart.pageobjects.AuthPage;
import com.curart.pageobjects.HomePage;
import com.curart.utilities.ReadConfig;

public class BaseClass
{
	ReadConfig readconfig = new ReadConfig();
	String url = readconfig.getUrl();
	String existingUser = readconfig.getExistingUser();
	String existingPwd = readconfig.getPassword();
	//String postcode = readconfig.getPostcode();
	
	public static WebDriver driver;
	public static Logger logger;
	
	public ExtentReporter extent;
	public ExtentTest extenttest;
	
	
	@Parameters("browser")
	@BeforeMethod
	public void setup(String br)
	{
		logger = Logger.getLogger("Currys Tests project");
		if(br.equals("chrome"))
		{
			//System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver");
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//Drivers//chromedriver.exe");
			driver = new ChromeDriver();
		}
		else if(br.equals("safari"))
		{
			driver = new SafariDriver();
		}
		else
		{
			System.out.println("Browser parameter not found");
		}
		
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.get(url);
	
	}
	
	public WebDriver signIn() throws InterruptedException
	{
		HomePage hp = new HomePage(driver);
		hp.acceptCookies();
		Thread.sleep(2000);
		hp.clickSignInLink();
		
		AuthPage ap = new AuthPage(driver);
		ap.getEmail(existingUser);
		ap.getPassword(existingPwd);
		ap.clickSubmit();

		return driver;
	}
	
	
	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException
	{
		if(result.getStatus()==ITestResult.FAILURE)
		{
			BaseClass.captureScreenshot(driver, result.getName());
		}
		
		driver.quit();
	}
	
	public static String captureScreenshot(WebDriver driver, String filename) throws IOException
	{
		File source = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		File target = new File(System.getProperty("user.dir")+ "/Screenshots/" + filename + ".jpg");
		FileUtils.copyFile(source, target);
		System.out.println("Test Failed, Screenshot taken");
		return System.getProperty("user.dir")+ "/Screenshots/" + filename + ".jpg";
	}

}
