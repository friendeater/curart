package com.curart.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage
{
	WebDriver ldriver;
	
	public HomePage(WebDriver rdriver)
	{
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(id = "onetrust-accept-btn-handler")
	WebElement cookiesAccept;
	
	@FindBy(xpath = "//*[@class='dc-header-item'][1]")
	WebElement SignInLink;
	
	@FindBy(xpath = "//*[@type='img' and @class='dc-header-logo--desktop']")
	WebElement CurrysLogo;
	
	public void acceptCookies()
	{
		cookiesAccept.click();
	}

	public void clickSignInLink()
	{
		SignInLink.click();
	}
	
	public void clickCurrysLogo()
	{
		CurrysLogo.click();
	}
	
}
