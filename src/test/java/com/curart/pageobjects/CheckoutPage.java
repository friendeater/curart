package com.curart.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutPage {

	WebDriver ldriver;
	
	public CheckoutPage(WebDriver rdriver)
	{
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(xpath = "//button[text()='Free']")
	WebElement flexibleFreeDelivery;
	
	@FindBy(id = "d-zip")
	WebElement postcodeBox;
	
	//@FindBy(xpath = "//button[@type='button']//*[text() = 'Find address']")
	@FindBy(xpath = "//*[text() = 'Find address']")
	WebElement postcodeSearchButton;
	
	@FindBy(xpath = "//li[@data-element = 'AddressesItem'][2]")
	WebElement address;
	
	@FindBy(xpath = "//button[@type='submit']//*[text() = 'Confirm & continue']")
	WebElement confirmAndContinue;
	
	@FindBy(xpath = "//*[@data-element = 'ChangeLocation']")
	WebElement changeLocation;
	
	@FindBy(id = "delivery_location")
	WebElement deliveryLocationBox;
	
	@FindBy(id = "dcg-icon-search")
	WebElement searchIcon;
	
	@FindBy(id = "dcg-icon-card")
	WebElement creditCardButton;
	
	public void flexibleFreeDeliveryClick()
	{
		flexibleFreeDelivery.click();
	}
	
	public void postcodeSearch() throws InterruptedException
	{
		postcodeSearchButton.click();
		Thread.sleep(3000);
		address.click();
		Thread.sleep(3000);
	}
	
	public void creditCardSelect()
	{
		creditCardButton.click();
	}
	
	
//	public void changeLocationclick() throws InterruptedException
//	{
//		Thread.sleep(4000);
//		changeLocation.click();
//		Thread.sleep(4000);
//		deliveryLocationBox.sendKeys("SP10 2AA");
//		Thread.sleep(4000);
//		searchIcon.click();
//		Thread.sleep(4000);
//	
//	}
	
	public void confirmAndContinueClick()
	{
		confirmAndContinue.click();
	}
}
