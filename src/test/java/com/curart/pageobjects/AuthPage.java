package com.curart.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AuthPage
{
	WebDriver ldriver;
	
	public AuthPage(WebDriver rdriver)
	{
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(name = "sEmail")
	WebElement email;
	
	@FindBy(id = "input-sPassword" )
	WebElement pass;
	
	@FindBy(xpath = "//*[@id='content']//button")
	WebElement submit;
	
	public void getEmail(String emailid)
	{
		email.sendKeys(emailid);
	}
	
	public void getPassword(String password)
	{
		pass.sendKeys(password);
	}
	
	public void clickSubmit()
	{
		submit.click();
	}
	
}
