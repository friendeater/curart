package com.curart.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPage {
	
	WebDriver ldriver;
	
	public ProductPage(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(xpath = "//*[@id=\"product-actions\"]//*[@class='space-b center']//*[contains(text(),'Add to basket')]")
	WebElement addToBasket;
	
	@FindBy(xpath="//*[@data-interaction='Continue to basket']")
	WebElement continueToBasket;
	
	public void addToBasketClick()
	{
		addToBasket.click();	
	}

	public void continueToBasket()
	{
		continueToBasket.click();
	}
	
}
