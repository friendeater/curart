package com.curart.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage {
	
	WebDriver ldriver;
	
	public MyAccountPage(WebDriver rdriver)
	{
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
	}
	
	
	@FindBy(id = "viewPersonalDetails")
	WebElement pdBox;
	
	@FindBy(id = "search")
	WebElement searchBox;
	
	@FindBy(xpath = "//*[@class='dc-button-wrapper']//*[text()='Search']")
	WebElement clickSearchSubmit;
	
	public void pdclick()
	{
		pdBox.click();
	}
	
	public void searchQuery(String query)
	{
		searchBox.sendKeys(query);
	}

	public void searchSubmitClick()
	{
		clickSearchSubmit.click();
	}
}
