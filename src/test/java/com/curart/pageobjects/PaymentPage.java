package com.curart.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaymentPage {

	WebDriver ldriver;
	
	public PaymentPage(WebDriver rdriver) {
		
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(id = "cancelButton")
	WebElement paymentCancelButton;
	
	
	public void cancelPaymentButtonClick()
	{
		paymentCancelButton.click();
	}
	
}
