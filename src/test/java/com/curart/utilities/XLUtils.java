package com.curart.utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XLUtils
{
	public static FileInputStream fi;
	public static FileOutputStream fo;
	public static XSSFWorkbook wb;
	public static XSSFSheet ws;
	public static XSSFRow row;
	public static XSSFCell cell;
	
	
	public static int getRowCount(String filename, String sheetname) throws IOException
	{
		int rowcount;
		fi = new FileInputStream(filename);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet(sheetname);
		rowcount = ws.getLastRowNum();
		wb.close();
		fi.close();
		return rowcount;
	}
	
	
	public static int getColCount(String filename, String sheetname, int rownum) throws IOException
	{
		int colcount;
		fi = new FileInputStream(filename);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet(sheetname);
		row = ws.getRow(rownum);
		colcount = row.getLastCellNum();
		wb.close();
		fi.close();
		return colcount;
	}
	
	public static String getCellData(String filename, String sheetname, int rownum, int colnum) throws IOException
	{
		fi = new FileInputStream(filename);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet(sheetname);
		row = ws.getRow(rownum);
		cell= row.getCell(colnum);
		
		DataFormatter formatter = new DataFormatter();
		String cellData = formatter.formatCellValue(cell);
		wb.close();
		fi.close();
		return cellData;
	}
}