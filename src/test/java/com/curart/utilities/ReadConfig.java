package com.curart.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig
{
	Properties pro;
	
	public ReadConfig()
	{
		try
		{
			File src = new File("./Configuration/config.properties");
			FileInputStream fis = new FileInputStream(src);
			pro = new Properties();
			pro.load(fis);
		}
		catch(Exception e)
		{
			System.out.println("File not found");
		}
	}
	
	public String getUrl()
	{
		String weburl = pro.getProperty("url");
		return weburl;
	}
	
	public String getExistingUser()
	{
		String existingUser = pro.getProperty("existingUser");
		return existingUser;
	}
	
	public String getPassword()
	{
		String pwd = pro.getProperty("existingPwd");
		return pwd;
	}
	
	public String getPostcode()
	{
		String postcode = pro.getProperty("postcode");
		return postcode;
	}
}
